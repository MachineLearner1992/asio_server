//
// Created by king on 18-12-19.
//

#ifndef ASIO_TEST_CTP_INFO_H
#define ASIO_TEST_CTP_INFO_H


#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <math.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <stack>
#include <mutex>
#include <set>


std::tuple<int, int> mod(int nor, int de);

std::vector<std::string> produceAllInstrumentNames(std::string instrumentType);

class InstrumentInfo{
public:
    InstrumentInfo(): instrumentTotalVol_(0) {}
    InstrumentInfo(std::string instrumentName, double instrumentTotalVol):
            instrumentName_(instrumentName), instrumentTotalVol_(instrumentTotalVol) {}

    InstrumentInfo(const InstrumentInfo& right):
            instrumentName_(right.instrumentName_), instrumentTotalVol_(right.instrumentTotalVol_) {}

    std::string getInstrumentName() {
        return instrumentName_;
    }

    double getInstrumentTotalVol() {
        return instrumentTotalVol_;
    }


    std::string instrumentName_;
    double instrumentTotalVol_;
};


class CTPInfo{
public:
    CTPInfo(std::string ctpAddr, std::string brockerId, std::string investorId, std::string passwd,
            std::vector<std::string> instrumentNames, std::string ipAddr, int port):
            ctpAddr_(ctpAddr), brockerId_(brockerId), investorId_(investorId),
            passwd_(passwd), instrumentTypeNames_(instrumentNames), ipAddr_(ipAddr), port_(port),
            IS_CHEKING_MAIN_INSTRAMENTS_(false), MAIN_INSTRUMENTS_CHECKED_(false) {
        strcpy(ctpAddrChar_, ctpAddr_.c_str());
    }

    std::set<std::string> prepareForCheckMainInstruments();

    void pushInstrument(InstrumentInfo info);

    void updateCheckedInstruments();

    std::set<std::string> getValidInstruments(){
        return validInstruments_;
    }

    char* getCtpAddr() {
        return ctpAddrChar_;
    }

    const std::string getBrockerId() {
        return brockerId_;
    }

    const std::string getInvestorId() {
        return investorId_;
    }

    const std::string getPasswd() {
        return passwd_;
    }

    const std::vector<std::string> getInstrumentTypeNames() {
        return instrumentTypeNames_;
    }

    const std::string getIpAddr() {
        return ipAddr_;
    }

    const int getPort() {
        return port_;
    }

    std::map<std::string, InstrumentInfo> getMainInstruments() {
        return mainInstruments_;
    }

protected:
    std::string ctpAddr_;
    char ctpAddrChar_[100];
    std::string brockerId_;
    std::string investorId_;
    std::string passwd_;
    std::vector<std::string> instrumentTypeNames_;
    std::string ipAddr_;
    int port_;
    std::set<std::string> alternativeInstruments_;
    std::set<std::string> validInstruments_;
    std::map<std::string, InstrumentInfo> mainInstruments_;

    std::set<std::string> checkedInstruments_;
    std::stack<InstrumentInfo> infoStack_;
    std::mutex lock_;

    bool IS_CHEKING_MAIN_INSTRAMENTS_;
    bool MAIN_INSTRUMENTS_CHECKED_;
};


#endif //ASIO_TEST_CTP_INFO_H
