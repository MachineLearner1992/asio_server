//
// Created by king on 18-12-25.
//

#ifndef ASIO_TEST_EVERYDAY_TIMER_HPP
#define ASIO_TEST_EVERYDAY_TIMER_HPP

#include <functional>
#include <boost/asio.hpp>
#include <time.h>
#include <memory>
#include "../small_tools/cplusplus/small_tools.h"

using namespace boost::asio;

class EveryDayTimeSchedular: public std::enable_shared_from_this<EveryDayTimeSchedular> {
public:
    EveryDayTimeSchedular(std::shared_ptr<io_service> pIoService, int hour, int minutes, long millisecond)
            :pIoService_(pIoService) {
        setTimer(hour, minutes, millisecond);
    }

    void setTimer(int hour, int minute, long millisecond) {
        hour_ = hour;
        minutes_ = minute;
        millisecond_ = millisecond;
#ifdef DEBUG
        std::cout << small_tools::makeTime(-1, -1, -1, hour, minute, millisecond) << std::endl;
#endif
        pTimer_ = std::make_shared<deadline_timer>(*(pIoService_),
                                                   small_tools::beijing2UTC(
                                                           small_tools::makeTime(-1, -1, -1, hour, minute, millisecond)));
    }

    void set_async_wait(std::function<void ()> f) {
        const boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
        callBackFunc_ = f;
        pTimer_->async_wait(std::bind(&EveryDayTimeSchedular::asyncWaitFunc, shared_from_this(), std::placeholders::_1));
    }

protected:
    void asyncWaitFunc(boost::system::error_code ec) {
        callBackFunc_();
        pTimer_->expires_from_now(boost::posix_time::hours(24));
        pTimer_->async_wait(std::bind(&EveryDayTimeSchedular::asyncWaitFunc, shared_from_this(), std::placeholders::_1));
    }

protected:
    std::shared_ptr<deadline_timer> pTimer_;
    std::shared_ptr<io_service> pIoService_;
    int hour_;
    int minutes_;
    long millisecond_;
    std::function<void ()> callBackFunc_;
};

#endif //ASIO_TEST_EVERYDAY_TIMER_HPP
