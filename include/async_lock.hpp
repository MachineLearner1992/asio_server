//
// Created by king on 19-1-10.
//

#ifndef ASIO_TEST_ASYNC_LOCK_HPP
#define ASIO_TEST_ASYNC_LOCK_HPP

class AsyncLock {
public:
    AsyncLock(): isLocked_(false) {}

    void getLock() {
        while(isLocked_) {}
        isLocked_ = true;
    }

    void freeLock() {
        isLocked_ = false;
    }

protected:
    bool isLocked_;
};

#endif //ASIO_TEST_ASYNC_LOCK_HPP
