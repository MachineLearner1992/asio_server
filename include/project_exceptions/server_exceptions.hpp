//
// Created by king on 18-12-2.
//

#ifndef ASIO_TEST_SERVER_EXCEPTIONS_HPP
#define ASIO_TEST_SERVER_EXCEPTIONS_HPP

#include <string>
#include <exception>

class MessageServerException: public std::exception{
public:
    explicit MessageServerException(std::string errorInfo):
    errorInfo_(errorInfo) {}

    virtual const char* what() const noexcept {
        return errorInfo_.c_str();
    }

protected:
    std::string errorInfo_;
};

#endif //ASIO_TEST_SERVER_EXCEPTIONS_HPP
