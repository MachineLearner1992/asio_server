//
// Created by king on 18-12-15.
//

#ifndef ASIO_TEST_GLOBAL_SINGLETON_H
#define ASIO_TEST_GLOBAL_SINGLETON_H

#include <boost/core/noncopyable.hpp>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <functional>

using namespace boost::asio;

class CThostFtdcMdApi;
class CThostFtdcMdSpi;
class MessageServer;
class CTPInfo;
class AsyncLock;


class GlobalSingleton: public boost::noncopyable{
public:
    static std::shared_ptr<GlobalSingleton> getInstance();

    GlobalSingleton* setPIoService(std::function<std::shared_ptr<io_service>()> f);
    GlobalSingleton* setPMessageServer(std::function<std::shared_ptr<MessageServer>()> f);
    GlobalSingleton* setPCTPInfo(std::function<std::shared_ptr<CTPInfo> ()> f);

    GlobalSingleton* setPUserApi(std::function<CThostFtdcMdApi* ()> f);
    GlobalSingleton* setPUserSpi(std::function<CThostFtdcMdSpi* ()> f);

    std::shared_ptr<io_service> getPIoService();
    std::shared_ptr<MessageServer> getPMessageServer();
    std::shared_ptr<CTPInfo> getPCTPInfo();

    std::shared_ptr<std::mutex> getGlobalLock();
    std::shared_ptr<AsyncLock> getGlobalAsyncLock();

    CThostFtdcMdApi* getPUserApi();
    CThostFtdcMdSpi* getPUserSpi();

protected:
    GlobalSingleton();

public:
    ~GlobalSingleton();
    void free_();

protected:
    static std::shared_ptr<GlobalSingleton> pGlobalSingleton_;
    static std::mutex mt_;

    std::shared_ptr<std::mutex> pGlobalLock_;
    std::shared_ptr<AsyncLock> pGlobalAsyncLock_;

    std::shared_ptr<io_service> pIoService_;
    std::shared_ptr<MessageServer> pMessageServer_;
    std::shared_ptr<CTPInfo> pCTPInfo_;

    CThostFtdcMdApi* pUserApi_;
    CThostFtdcMdSpi* pUserSpi_;
};


#endif //ASIO_TEST_GLOBAL_SINGLETON_H
