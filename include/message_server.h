//
// Created by king on 18-11-28.
//

#ifndef ASIO_TEST_MESSAGE_SERVER_H
#define ASIO_TEST_MESSAGE_SERVER_H

#include <iostream>
#include <functional>
#include <tuple>
#include <vector>
#include <boost/asio.hpp>
#include <time.h>
#include <map>
#include <string>
#include <memory>
#include <list>
#include <thread>
#include "project_exceptions/server_exceptions.hpp"

#include "../small_tools/cplusplus/small_tools.h"

using namespace boost::asio;


class Session: public std::enable_shared_from_this<Session>{
public:
    Session(std::shared_ptr<io_service> pioService, std::shared_ptr<ip::tcp::socket> psocket):
            pIoService_(pioService), pSocket_(psocket), IS_ALIVE_(true) {}

    void send_async(std::shared_ptr<std::string> message);

    bool checkAlive() {return IS_ALIVE_;}

protected:
    void writeHandler(std::shared_ptr<std::string> pstr, boost::system::error_code ec, size_t bytesTransffered);

protected:
    std::shared_ptr<io_service> pIoService_;
    std::shared_ptr<ip::tcp::socket> pSocket_;
    bool IS_ALIVE_;
};


class MessageServer: public std::enable_shared_from_this<MessageServer>, boost::noncopyable{
public:
    MessageServer(std::shared_ptr<io_service> pIoService, std::string ipAddress,
            unsigned short port): pIoService_(pIoService) {
        setAddress(ipAddress, port);
    }

    void setAddress(std::string ipAddress, unsigned short port);

    void listen();

    const std::shared_ptr<boost::asio::io_service> getIoService();

    void sendToAllSessions(std::shared_ptr<std::string>  pstr);

protected:
    void acceptHandler(std::shared_ptr<ip::tcp::socket> psocket, boost::system::error_code ec);

protected:
    std::shared_ptr<io_service> pIoService_;
    std::shared_ptr<ip::tcp::acceptor> pAcceptor_;
    std::list<std::shared_ptr<Session>> pSessionList_;

};


#endif //ASIO_TEST_MESSAGE_SERVER_H
