//
// Created by king on 18-12-23.
//

#include "../include/message_server.h"

void Session::send_async(std::shared_ptr<std::string> message){
    pSocket_->async_write_some(buffer(*message),
                               std::bind(&Session::writeHandler, shared_from_this(),
                                         message, std::placeholders::_1, std::placeholders::_2));
}


void Session::writeHandler(std::shared_ptr<std::string> pstr, boost::system::error_code ec, size_t bytesTransffered){
    if(ec){
        std::cout << "error failed, error msg is :" << ec << std::endl;
        pSocket_->close();
        IS_ALIVE_ = false;
    }
    else{
        try {
            boost::asio::ip::address addr = pSocket_->remote_endpoint().address();
//            std::cout << *pstr << " send succeed to " << addr << std::endl;
            std::cout << " send succeed to " << addr << ", information length is " << pstr->length() << std::endl;
        }
        catch(std::exception &e){
            std::cout << e.what() << std::endl;
            pSocket_->close();
            IS_ALIVE_ = false;
        }
    }
}


void MessageServer::setAddress(std::string ipAddress, unsigned short port){
    std::cout << "server address is: " << ipAddress << " port is: " << port << std::endl;
    pAcceptor_ = std::shared_ptr<ip::tcp::acceptor>(
            new ip::tcp::acceptor(*pIoService_,
                                  ip::tcp::endpoint(ip::address::from_string(ipAddress), port)));
//    pAcceptor_ = std::make_shared<ip::tcp::acceptor> (*pIoService_,
//                                                      ip::tcp::endpoint(ip::address::from_string(ipAddress), port));
}


void MessageServer::listen(){
    if(pAcceptor_) {
        std::shared_ptr<ip::tcp::socket> psocket(new ip::tcp::socket(*pIoService_));
        pAcceptor_->async_accept(*psocket,
                                 std::bind(&MessageServer::acceptHandler, shared_from_this(), psocket,
                                           std::placeholders::_1));
    }
    else{
        throw MessageServerException("set ip address before listen");
    }
}


const std::shared_ptr<boost::asio::io_service> MessageServer::getIoService(){
    return pIoService_;
}


void MessageServer::sendToAllSessions(std::shared_ptr<std::string>  pstr){
    for(auto psessIter = pSessionList_.begin(); psessIter != pSessionList_.end();){
        if((*psessIter)->checkAlive()){
            (*psessIter)->send_async(pstr);
            psessIter++;
        }
        else{
            psessIter = pSessionList_.erase(psessIter);
        }
    }
}

void MessageServer::acceptHandler(std::shared_ptr<ip::tcp::socket> psocket, boost::system::error_code ec){
    if(ec){
        std::cout << "can not accept, the error code is " << ec << std::endl;
    }
    else{
        std::cout << "accept from addr " << psocket->remote_endpoint().address() << std::endl;
        pSessionList_.push_back(std::make_shared<Session>(pIoService_, psocket));
    }
    listen();
}
