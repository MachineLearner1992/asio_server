
#pragma warning(disable : 4996)

#include <iostream>
#include <stdlib.h>
#include <memory.h>
#include "../include/MdSpi.h"
#include "../include/global_singleton.h"
#include "../include/ctp_info.h"
#include "../include/message_server.h"
#include <thread>

#include "../protobuf_file/gen_code/interface.pb.h"
#include "../protobuf_file/gen_code/user_config.pb.h"

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include <dirent.h>

using namespace std;

 //USER_API����
//extern CThostFtdcMdApi* pUserApi;
//no use

// ���ò���
extern char FRONT_ADDR[];
extern TThostFtdcBrokerIDType	BROKER_ID;
extern TThostFtdcInvestorIDType INVESTOR_ID;
extern TThostFtdcPasswordType	PASSWORD;
extern char* ppInstrumentID[];
extern int iInstrumentID;


Price genFullProtobufPriceFromMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData, bool isMainInstrument) {
	Price p;
	p.set_tradingday(pDepthMarketData->TradingDay);
	p.set_instrumentid(pDepthMarketData->InstrumentID);
	p.set_exchangeid(pDepthMarketData->ExchangeID);
	p.set_exchangeinstid(pDepthMarketData->ExchangeInstID);
	p.set_lastprice(pDepthMarketData->LastPrice);
	p.set_presettlementprice(pDepthMarketData->PreSettlementPrice);
	p.set_preopeninterest(pDepthMarketData->PreOpenInterest);
	p.set_openprice(pDepthMarketData->OpenPrice);
	p.set_highestprice(pDepthMarketData->HighestPrice);
	p.set_lowestprice(pDepthMarketData->LowestPrice);
	p.set_volume(pDepthMarketData->Volume);
	p.set_turnover(pDepthMarketData->Turnover);
	p.set_openinterest(pDepthMarketData->OpenInterest);
	p.set_closeprice(pDepthMarketData->ClosePrice);
	p.set_settlementprice(pDepthMarketData->SettlementPrice);
	p.set_upperlimitprice(pDepthMarketData->UpperLimitPrice);
	p.set_lowerlimitprice(pDepthMarketData->LowerLimitPrice);
	p.set_predelta(pDepthMarketData->PreDelta);
	p.set_currdelta(pDepthMarketData->CurrDelta);
	p.set_updatetime(pDepthMarketData->UpdateTime);
	p.set_updatemillisec(pDepthMarketData->UpdateMillisec);
	p.set_bidprice1(pDepthMarketData->BidPrice1);
	p.set_bidvolume1(pDepthMarketData->BidVolume1);
	p.set_askprice1(pDepthMarketData->AskPrice1);
	p.set_askvolume1(pDepthMarketData->AskVolume1);
	p.set_bidprice2(pDepthMarketData->BidPrice2);
	p.set_bidvolume2(pDepthMarketData->BidVolume2);
	p.set_askprice2(pDepthMarketData->AskPrice2);
	p.set_askvolume2(pDepthMarketData->AskVolume2);
	p.set_bidprice3(pDepthMarketData->BidPrice3);
	p.set_bidvolume3(pDepthMarketData->BidVolume3);
	p.set_askprice3(pDepthMarketData->AskPrice3);
	p.set_askvolume3(pDepthMarketData->AskVolume3);
	p.set_bidprice4(pDepthMarketData->BidPrice4);
	p.set_bidvolume4(pDepthMarketData->BidVolume4);
	p.set_askprice4(pDepthMarketData->AskPrice4);
	p.set_askvolume4(pDepthMarketData->AskVolume4);
	p.set_bidprice5(pDepthMarketData->BidPrice5);
	p.set_bidvolume5(pDepthMarketData->BidVolume5);
	p.set_askprice5(pDepthMarketData->AskPrice5);
	p.set_askvolume5(pDepthMarketData->AskVolume5);
	p.set_averageprice(pDepthMarketData->AveragePrice);
	p.set_actionday(pDepthMarketData->ActionDay);
	p.set_is_main_instrument(isMainInstrument);
	return p;
}

// ������

void CMdSpi::OnRspError(CThostFtdcRspInfoField *pRspInfo,
		int nRequestID, bool bIsLast) {
	cerr << "--->>> "<< __FUNCTION__ << endl;
	IsErrorRspInfo(pRspInfo);
}

void CMdSpi::OnFrontDisconnected(int nReason) {
	cerr << "--->>> " << __FUNCTION__ << endl;
	cerr << "--->>> Reason = " << nReason << endl;
}
		
void CMdSpi::OnHeartBeatWarning(int nTimeLapse) {
	cerr << "--->>> " << __FUNCTION__ << endl;
	cerr << "--->>> nTimerLapse = " << nTimeLapse << endl;
}

void CMdSpi::OnFrontConnected() {
	cerr << "--->>> " << __FUNCTION__ << endl;
	// after connected proceed login
	ReqUserLogin();
}

void CMdSpi::ReqUserLogin() {
	std::shared_ptr<GlobalSingleton> singleton = GlobalSingleton::getInstance();
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, singleton->getPCTPInfo()->getBrockerId().c_str());
	strcpy(req.UserID, singleton->getPCTPInfo()->getInvestorId().c_str());
	strcpy(req.Password, singleton->getPCTPInfo()->getPasswd().c_str());
	int iResult = pUserApi->ReqUserLogin(&req, ++iRequestID_);
	cerr << "--->>> the login request send: " << ((iResult == 0) ? "succeed" : "failed") << endl;
}


void CMdSpi::ReqUserLogout() {
	std::shared_ptr<GlobalSingleton> singleton = GlobalSingleton::getInstance();
	CThostFtdcUserLogoutField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, singleton->getPCTPInfo()->getBrockerId().c_str());
	strcpy(req.UserID, singleton->getPCTPInfo()->getInvestorId().c_str());
	int iResult = pUserApi->ReqUserLogout(&req, ++iRequestID_);
	cerr << "--->>> the logout request send: " << ((iResult == 0) ? "succeed" : "failed") << endl;
}

void CMdSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,
		CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {
	cerr << "--->>> " << __FUNCTION__ << endl;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		// if login succeed get the trading date
		cerr << "--->>> the trading date = " << pUserApi->GetTradingDay() << endl;
		// subscribe the market data
		SubscribeMarketData();
	}
}


void CMdSpi::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, 
							 CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {
	cerr << "--->>> " << __FUNCTION__ << endl;
	std::cout << "logout successful" << std::endl;
}


void CMdSpi::SubscribeMarketData() {
	std::shared_ptr<GlobalSingleton> singleton = GlobalSingleton::getInstance();
	std::shared_ptr<CTPInfo> ctpInfo = singleton->getPCTPInfo();
    if(IS_UPDATE_INFO_) {
        std::set<std::string> alteredInstruments = ctpInfo->prepareForCheckMainInstruments();
        char** ppInstrumentIDLocal = new char*[alteredInstruments.size()];
        memset(ppInstrumentIDLocal, 0, alteredInstruments.size() * sizeof(char*));

        int iter = 0;
        for(const std::string el: alteredInstruments) {
        	ppInstrumentIDLocal[iter] = new char[10];
            strcpy(ppInstrumentIDLocal[iter], el.c_str());
            iter++;
        }
		int iResult = pUserApi->SubscribeMarketData(ppInstrumentIDLocal, alteredInstruments.size());
		cerr << "--->>> subscribe market data request send: " << ((iResult == 0) ? "succeed" : "failed") << endl;

		for(int i = 0; i < alteredInstruments.size(); i++) {

//#ifdef DEBUG
//			SHOW_CODE_INFO();
//			std::cout << "checked instruments" << ppInstrumentIDLocal[i] << std::endl;
//#endif
			delete [] ppInstrumentIDLocal[i];
		}
		delete [] ppInstrumentIDLocal;
    }
    else {
//		int iResult = pUserApi->SubscribeMarketData(ppInstrumentID, iInstrumentID);
//		cerr << "--->>> subscribe market data request send: " << ((iResult == 0) ? "succeed" : "failed") << endl;
		std::set<std::string> validInstruments = ctpInfo->getValidInstruments();
		std::cerr << "the size of valid instruments is " << validInstruments.size() << std::endl;
		char** ppInstrumentIDLocal = new char*[validInstruments.size()];
		memset(ppInstrumentIDLocal, 0, validInstruments.size() * sizeof(char*));

		int iter = 0;
		for(const std::string el: validInstruments) {
			ppInstrumentIDLocal[iter] = new char[10];
			strcpy(ppInstrumentIDLocal[iter], el.c_str());
			iter++;
		}
		int iResult = pUserApi->SubscribeMarketData(ppInstrumentIDLocal, validInstruments.size());
		cerr << "--->>> subscribe market data request send: " << ((iResult == 0) ? "succeed" : "failed") << endl;

		for(int i = 0; i < validInstruments.size(); i++) {
			delete [] ppInstrumentIDLocal[i];
		}
		delete [] ppInstrumentIDLocal;
	}
}


void CMdSpi::UnSubscribeMarketData() {
	int iResult = pUserApi->UnSubscribeMarketData(ppInstrumentID, iInstrumentID);
	cerr << "--->>> unsubscribe market data request send: " << ((iResult == 0) ? "succeed" : "failed") << endl;
}


void CMdSpi::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument,
		CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {
	if (bIsLast && !IsErrorRspInfo(pRspInfo)) {
		cerr << __FUNCTION__ << endl;
		std::cout << "subscribe instrument: " << pSpecificInstrument->InstrumentID << " succeed" << std::endl;
	}
	else{
	}
}

void CMdSpi::OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument,
		CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) {
	cerr << __FUNCTION__ << endl;
	if (bIsLast && !IsErrorRspInfo(pRspInfo)) {
		std::cout << "unsubscribe instrument: " << pSpecificInstrument->InstrumentID << "succeed" << std::endl;
	}
}

void CMdSpi::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData) {
	std::shared_ptr<GlobalSingleton> singleton = GlobalSingleton::getInstance();
	std::shared_ptr<CTPInfo> ctpInfo = singleton->getPCTPInfo();

    if(IS_UPDATE_INFO_) {
//#ifdef DEBUG
//		cerr << __FUNCTION__ << endl;
//		std::cout << "the thread id is in " << __FUNCTION__  << "is " << std::this_thread::get_id() << std::endl;
//		std::cout << "pushing instrument " << pDepthMarketData->InstrumentID << " info" << std::endl;
//#endifdd
		std::string InstrumentID = pDepthMarketData->InstrumentID;
		TThostFtdcLargeVolumeType PreOpenInterest = pDepthMarketData->PreOpenInterest;
		ctpInfo->pushInstrument(InstrumentInfo(InstrumentID, PreOpenInterest));
    }
    else {
//		cerr << __FUNCTION__ << endl;
		std::shared_ptr<MessageServer> pMessageServer = singleton->getPMessageServer();
		std::string instrumentId = pDepthMarketData->InstrumentID;
		std::string instrumentType(instrumentId.begin(), instrumentId.begin() + 2);
		std::map<std::string, InstrumentInfo> mainInstruments = singleton->getPCTPInfo()->getMainInstruments();

		bool isMainInstrument = false;
		if(0 == instrumentId.compare(mainInstruments[instrumentType].instrumentName_)) {
		    isMainInstrument = true;
		}

		Price p = genFullProtobufPriceFromMarketData(pDepthMarketData, isMainInstrument);
		shared_ptr<string> pstr(new string);
		p.SerializeToString(pstr.get());
		pMessageServer->sendToAllSessions(pstr);
	}
}

bool CMdSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo) {
	// if ErrorID != 0, there exists some error
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
		cerr << "--->>> ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << endl;
	return bResult;
}