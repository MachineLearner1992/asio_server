//
// Created by king on 18-12-19.
//

#include "../include/ctp_info.h"
#include "../small_tools/cplusplus/small_tools.h"


std::tuple<int, int> mod(int nor, int de){
    int div = static_cast<int>(static_cast<float>(nor) / static_cast<float>(de));
    int remain = nor - de * div;
    return std::make_tuple(div, remain);
}

std::vector<std::string> produceAllInstrumentNames(std::string instrumentType){
    const boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
    boost::gregorian::date nowDate = now.date();

    std::vector<std::string> allInstrumentNames(12);
    for(int monthDiff = 0; monthDiff < 12; monthDiff++){
        boost::gregorian::date newDate = nowDate + boost::gregorian::months(monthDiff);
        int newYear = std::get<1>(mod(newDate.year(), 1000));
        int newMonth = newDate.month();
        char nameBuffer[20] = {};

        sprintf(nameBuffer, "%s%.2d%.2d", instrumentType.c_str(), newYear, newMonth);
        std::string instrumentName(nameBuffer);
        allInstrumentNames[monthDiff] = instrumentName;
    }
    return allInstrumentNames;
}


std::set<std::string> CTPInfo::prepareForCheckMainInstruments(){
        alternativeInstruments_.clear();
        validInstruments_.clear();
        mainInstruments_.clear();
        mainInstruments_.clear();

        checkedInstruments_.clear();
        std::stack<InstrumentInfo>().swap(infoStack_); // clear the stack

        for(int i = 0; i < instrumentTypeNames_.size(); i++) {
            std::vector<std::string> tempInstruments = produceAllInstrumentNames(instrumentTypeNames_[i]);
            for(int j = 0; j < tempInstruments.size(); j++) {
                alternativeInstruments_.insert(tempInstruments[j]);
            }
        }

#ifdef DEBUG
        SHOW_CODE_INFO();
        std::cout << "alternative instruments area: " << std::endl;
        for(auto tt: alternativeInstruments_) {
            std::cout << tt << ",";
        }
        std::cout << std::endl;
#endif

        return alternativeInstruments_;
}



void CTPInfo::pushInstrument(InstrumentInfo info) {

    if(checkedInstruments_.find(info.instrumentName_) == checkedInstruments_.end()) {
        lock_.lock();
        checkedInstruments_.insert(info.instrumentName_);
        infoStack_.push(info);
        lock_.unlock();
    }
}


void CTPInfo::updateCheckedInstruments() {
    lock_.lock();
    while(!infoStack_.empty()) {
        InstrumentInfo temp = infoStack_.top();
        std::string instrumentTypeName(temp.instrumentName_.begin(), temp.instrumentName_.begin() + 2);
        if(temp.instrumentTotalVol_ > 0) {
            validInstruments_.insert(temp.instrumentName_);
        }
#ifdef DEBUG
        else{
            std::cout << temp.instrumentName_ << "has total vol of " << temp.instrumentTotalVol_ << std::endl;
        }
#endif
        if(mainInstruments_.find(instrumentTypeName) != mainInstruments_.end()) {
            if (mainInstruments_[instrumentTypeName].instrumentTotalVol_ < temp.instrumentTotalVol_) {
                mainInstruments_[instrumentTypeName] = temp;
            }
        }
        else{
            mainInstruments_[instrumentTypeName] = temp;
        }
        infoStack_.pop();
    }
    lock_.unlock();
}

