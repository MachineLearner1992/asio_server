//
// Created by king on 18-12-15.
//

#include "../include/global_singleton.h"
#include "../include/MdSpi.h"
#include "../include/project_exceptions/server_exceptions.hpp"
#include "../include/ctp_info.h"
#include "../include/async_lock.hpp"
#include "../small_tools/cplusplus/small_tools.h"


//important for singleton object to declare the reference for the static variable
std::shared_ptr<GlobalSingleton> GlobalSingleton::pGlobalSingleton_;
std::mutex GlobalSingleton::mt_;


GlobalSingleton::GlobalSingleton(): pUserApi_(nullptr), pUserSpi_(nullptr) {
    pGlobalLock_ = std::make_shared<std::mutex>();
    pGlobalAsyncLock_ = std::make_shared<AsyncLock>();
}


GlobalSingleton::~GlobalSingleton() {
    free_();
}


std::shared_ptr<GlobalSingleton> GlobalSingleton::getInstance() {
    if(!pGlobalSingleton_){
        mt_.lock();
        if(!pGlobalSingleton_) {
            // can not use make_shared because constructor is protected in singleton object
            pGlobalSingleton_ = std::shared_ptr<GlobalSingleton>(new GlobalSingleton);
        }
        mt_.unlock();
    }
    return pGlobalSingleton_;
}


void GlobalSingleton::free_() {

#ifdef DEBUG
    SHOW_CODE_INFO();
    std::cout << "free ctp pointer" << std::endl;
#endif

    if(pUserApi_ != nullptr)
    {
        pUserApi_->RegisterSpi(nullptr);
        pUserApi_->Release();
        pUserApi_ = nullptr;
    }

    if(pUserSpi_ != nullptr)
    {
        delete pUserSpi_;
        pUserSpi_ = nullptr;
    }
}


GlobalSingleton* GlobalSingleton::setPIoService(std::function<std::shared_ptr<io_service>()> f){
    pIoService_ = f();
    return this;
}


GlobalSingleton* GlobalSingleton::setPMessageServer(std::function<std::shared_ptr<MessageServer>()> f) {
    pMessageServer_ = f();
    return this;
}


GlobalSingleton* GlobalSingleton::setPCTPInfo(std::function<std::shared_ptr<CTPInfo>()> f) {
    pCTPInfo_ = f();
    return this;
}


GlobalSingleton* GlobalSingleton::setPUserApi(std::function<CThostFtdcMdApi* ()> f){
    pUserApi_ = f();
    return this;
}


GlobalSingleton* GlobalSingleton::setPUserSpi(std::function<CThostFtdcMdSpi* ()> f){
    pUserSpi_ = f();
    return this;
}


std::shared_ptr<io_service> GlobalSingleton::getPIoService(){
    if(!pIoService_){
        throw MessageServerException("must initialize asio io service in singleton object");
    }
    return pIoService_;
}


std::shared_ptr<MessageServer> GlobalSingleton::getPMessageServer() {
    if(!pMessageServer_){
        throw MessageServerException("must initialize message server in singleton object");
    }
    return pMessageServer_;
}


std::shared_ptr<CTPInfo> GlobalSingleton::getPCTPInfo() {
    if(!pCTPInfo_){
        throw MessageServerException("must initialize ctp info in singleton object");
    }
    return pCTPInfo_;
}


CThostFtdcMdApi* GlobalSingleton::getPUserApi(){
    if(!pUserApi_) {
        throw MessageServerException("must initialize user api in singleton object");
    }
    return pUserApi_;
}


CThostFtdcMdSpi* GlobalSingleton::getPUserSpi(){
    if(!pUserSpi_){
        throw MessageServerException("must initialize user spi in singleton object");
    }
    return pUserSpi_;
}


std::shared_ptr<std::mutex> GlobalSingleton::getGlobalLock() {
    return pGlobalLock_;
}


std::shared_ptr<AsyncLock> GlobalSingleton::getGlobalAsyncLock() {
    return pGlobalAsyncLock_;
}
