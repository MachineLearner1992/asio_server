#include "protobuf_file/gen_code/interface.pb.h"
#include "protobuf_file/gen_code/user_config.pb.h"

#include "include/MdSpi.h"
#include "include/ctp/ThostFtdcMdApi.h"
#include "include/global_singleton.h"
#include "include/ctp_info.h"
#include "include/message_server.h"
#include "include/everyday_timer.hpp"
#include "include/async_lock.hpp"

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>
#include <dirent.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/timer.hpp>
#include <functional>
#include <csignal>
#include <thread>

using namespace std;


Config readConfigFromPrototxt(std::string filePath){
    int fd = open(filePath.c_str(), O_RDONLY);
    if(-1 == fd){
        throw MessageServerException("config file not found");
    }
    std::shared_ptr<google::protobuf::io::FileInputStream> input(new google::protobuf::io::FileInputStream(fd));
    Config config;
    bool success = google::protobuf::TextFormat::Parse(input.get(), &config);
    if(success < 0){
        throw MessageServerException("config file read fail");
    }

    close(fd);
    return config;
}


char FRONT_ADDR[] = "tcp://61.186.254.135:41213";		// ǰ�õ�ַ
TThostFtdcBrokerIDType	BROKER_ID = "9080";				// ���͹�˾����
TThostFtdcInvestorIDType INVESTOR_ID = "10909079";			// Ͷ���ߴ���
TThostFtdcPasswordType  PASSWORD = "072138";			// �û�����
char *ppInstrumentID[] = {"ag1911"};			// ���鶩���б�
int iInstrumentID = 1;									// ���鶩������

using namespace boost::gregorian;

int main(int argc, char* argv[]) {
    if(argc < 2){
        throw MessageServerException("enter server config file as first argument");
    }
    else{
        const int MAX_PATH = 1000;
        char buffer[MAX_PATH];
        getcwd(buffer, MAX_PATH);
        std::string currentDir = std::string(buffer);
        std::cout << "current program dir is: " << currentDir << std::endl;
        std::cout << "config file is: " << argv[1] << std::endl;
    }

    Config config = readConfigFromPrototxt(std::string(argv[1]));
    std::string ctpAddr = config.ctp_addr();
    std::string brockerId = config.broker_id();
    std::string investorId = config.instrument_types();
    std::vector<std::string> instrumentsTypes = small_tools::stringSplit(config.instrument_types(), ",");
    std::string passwd = config.passwd();
    std::string ipAddress = config.ip_address();
    int port = config.port();

    std::shared_ptr<GlobalSingleton> singleton = GlobalSingleton::getInstance();
    singleton->setPIoService(
                    [=]() -> std::shared_ptr<io_service> {
                        return std::make_shared<io_service>();
                    })
            ->setPCTPInfo(
                    [=]() -> std::shared_ptr<CTPInfo> {
                        std::shared_ptr<CTPInfo> pCtpInfo = std::make_shared<CTPInfo>(
                                ctpAddr, brockerId, investorId,  passwd, instrumentsTypes, ipAddress, port);
                        return pCtpInfo;
                    });

    std::function<void ()> construmentMainInstrumentsFunc =
            [=] () -> void {

                singleton->free_();
                singleton->setPUserApi(
                        [=]() -> CThostFtdcMdApi* {
                            return CThostFtdcMdApi::CreateFtdcMdApi("./CTPFile", false);
                        })->setPUserSpi(
                        [=]() -> CThostFtdcMdSpi* {
                            CThostFtdcMdApi* puserApi = singleton->getPUserApi();
                            return new CMdSpi(puserApi, true);
                        });
                singleton->getPUserApi()->RegisterSpi(singleton->getPUserSpi());
                singleton->getPUserApi()->RegisterFront(singleton->getPCTPInfo()->getCtpAddr());					// connect
                singleton->getPUserApi()->Init();
                sleep(3);
                singleton->getPCTPInfo()->updateCheckedInstruments();
                singleton->free_();

                singleton->setPUserApi(
                        [=]() -> CThostFtdcMdApi* {
                            return CThostFtdcMdApi::CreateFtdcMdApi("./CTPFile", false);
                        })->setPUserSpi(
                        [=]() -> CThostFtdcMdSpi* {
                            CThostFtdcMdApi* puserApi = singleton->getPUserApi();
                            return new CMdSpi(puserApi, false);
                        });
                singleton->getPUserApi()->RegisterSpi(singleton->getPUserSpi());
                singleton->getPUserApi()->RegisterFront(singleton->getPCTPInfo()->getCtpAddr());					// connect
                singleton->getPUserApi()->Init();

            };

    construmentMainInstrumentsFunc();
    std::shared_ptr<EveryDayTimeSchedular> everyDayTimeSchedular =
            std::make_shared<EveryDayTimeSchedular>(singleton->getPIoService(), 0, 50, 0 );
    everyDayTimeSchedular->set_async_wait(construmentMainInstrumentsFunc);

    singleton->setPMessageServer(
            [=]() -> std::shared_ptr<MessageServer> {
                std::shared_ptr<MessageServer> pMassageServer =
                        std::make_shared<MessageServer>(singleton->getPIoService(),
                                                        singleton->getPCTPInfo()->getIpAddr(), singleton->getPCTPInfo()->getPort());
                return pMassageServer;
            });
    std::shared_ptr<MessageServer> pMessageServer = singleton->getPMessageServer();
    pMessageServer->listen();

    singleton->getPIoService()->run();
    return 0;
}

